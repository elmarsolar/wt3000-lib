﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Globals" Type="Folder"/>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="GetLastError codes.ctl" Type="VI" URL="../Private/Typedefs/GetLastError codes.ctl"/>
		</Item>
		<Item Name="Error codes converter.vi" Type="VI" URL="../Private/Error codes converter.vi"/>
		<Item Name="Get data.vi" Type="VI" URL="../Private/Get data.vi"/>
		<Item Name="Get waveform data per trace.vi" Type="VI" URL="../Private/Get waveform data per trace.vi"/>
		<Item Name="Queries Data.vi" Type="VI" URL="../Private/Queries Data.vi"/>
		<Item Name="Send only.vi" Type="VI" URL="../Private/Send only.vi"/>
		<Item Name="Set item settings.vi" Type="VI" URL="../Private/Set item settings.vi"/>
		<Item Name="String to array.vi" Type="VI" URL="../Private/String to array.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="Connection settings.ctl" Type="VI" URL="../Public/Typedefs/Connection settings.ctl"/>
			<Item Name="Current range.ctl" Type="VI" URL="../Public/Typedefs/Current range.ctl"/>
			<Item Name="Element range.ctl" Type="VI" URL="../Public/Typedefs/Element range.ctl"/>
			<Item Name="Update rate range.ctl" Type="VI" URL="../Public/Typedefs/Update rate range.ctl"/>
			<Item Name="Voltage range.ctl" Type="VI" URL="../Public/Typedefs/Voltage range.ctl"/>
		</Item>
		<Item Name="Connect.vi" Type="VI" URL="../Public/Connect.vi"/>
		<Item Name="Disconnect.vi" Type="VI" URL="../Public/Disconnect.vi"/>
		<Item Name="Get data (single entry).vi" Type="VI" URL="../Public/Get data (single entry).vi"/>
		<Item Name="Get data by update range.vi" Type="VI" URL="../Public/Get data by update range.vi"/>
		<Item Name="Get Ranges.vi" Type="VI" URL="../Public/Get Ranges.vi"/>
		<Item Name="Get update rate.vi" Type="VI" URL="../Public/Get update rate.vi"/>
		<Item Name="Get waveform data.vi" Type="VI" URL="../Public/Get waveform data.vi"/>
		<Item Name="Get-set receive monitor.vi" Type="VI" URL="../Public/Get-set receive monitor.vi"/>
		<Item Name="Get-set send monitor.vi" Type="VI" URL="../Public/Get-set send monitor.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="Send string message.vi" Type="VI" URL="../Public/Send string message.vi"/>
		<Item Name="Set update rate.vi" Type="VI" URL="../Public/Set update rate.vi"/>
		<Item Name="Set voltage and current.vi" Type="VI" URL="../Public/Set voltage and current.vi"/>
		<Item Name="Set waveform settings.vi" Type="VI" URL="../Public/Set waveform settings.vi"/>
		<Item Name="Timer1 tick.vi" Type="VI" URL="../Public/Timer1 tick.vi"/>
	</Item>
</Library>
