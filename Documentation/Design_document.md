# Design document of the WT3000 library
This library is developed by Goaldigger, commissioned by Elmar Solar, to bridge between the WT3000 TMCTL library and Demo LabVIEW software to show the efficiency of the Elmar Solar MPPT's.

Copyright (R) - Michael ten Den, Goaldigger - April 2019

## Functions flow

### Get data of single entry

1. Send item settings
2. Get item data


### Get waveform data

1. Set settings waveform (ASCII, rate, etc) - one time
2. Set Trace
3. Read waveform data (:WAVEFORM:SEND?) (depending on rate)
4. Multiple items in Trace array, then select next item and repeat 2-4



## Cheatsheet WT3000 commando's
When asking for settings, add ? directly after the text
When writing settings, add a space and then the values

:DISPLAY:MODE
:DISPLAY:WAVE:TDIV
:DISPLAY:WAVE:{U<x>|I<x>|MATH<x>}
:NUMERIC:FORMAT ASCII
:RATE 50MS
:WAVEFORM?
:WAVEFORM:TRACE {U<x>|I<x>|MATH<x>}
:WAVEFORM:SEND?
:WAVEFORM:FORMAT ASCII

