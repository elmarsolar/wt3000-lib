﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="External libs" Type="Folder">
			<Item Name="tmctl64.lvlib" Type="Library" URL="/&lt;userlib&gt;/tmctl64/tmctl64.lvlib"/>
		</Item>
		<Item Name="Test" Type="Folder">
			<Item Name="Global" Type="Folder">
				<Item Name="Shutdown global.vi" Type="VI" URL="../Test/Global/Shutdown global.vi"/>
			</Item>
			<Item Name="SubVIs" Type="Folder">
				<Item Name="Set frontpanel settings.vi" Type="VI" URL="../Test/Subvi/Set frontpanel settings.vi"/>
				<Item Name="Timestamp with error line.vi" Type="VI" URL="../Test/Subvi/Timestamp with error line.vi"/>
			</Item>
			<Item Name="State_machine.ctl" Type="VI" URL="../Test/State_machine.ctl"/>
			<Item Name="Test - Connect and disconnect.vi" Type="VI" URL="../Test/Test - Connect and disconnect.vi"/>
			<Item Name="Test - Event based WT3000 communication.vi" Type="VI" URL="../Test/Test - Event based WT3000 communication.vi"/>
			<Item Name="Test - Get ranges.vi" Type="VI" URL="../Test/Test - Get ranges.vi"/>
			<Item Name="Test - Read data.vi" Type="VI" URL="../Test/Test - Read data.vi"/>
		</Item>
		<Item Name="WT3000 Lib.lvlib" Type="Library" URL="../WT3000 Lib.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Clear All Errors__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Clear All Errors__ogtk.vi"/>
				<Item Name="Wait (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Wait (ms)__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="My Packed Library" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F1810A02-EACA-4B79-AECB-7158B58E350C}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">My Packed Library</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Packed lib builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{A0030897-CEB2-429E-96A1-6730FBFE10F9}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">WT3000 Lib.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../Packed lib builds/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Packed lib builds</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{3BCAC53D-9B48-43C4-9E8C-E615DDFDCF90}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/WT3000 Lib.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Goaldigger</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Packed library for the WT3000 using TMCTL.dll</Property>
				<Property Name="TgtF_internalName" Type="Str">WT3000 Lib</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Goal-digger.nl © 2019 </Property>
				<Property Name="TgtF_productName" Type="Str">WT3000 - Packed Library </Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{478AAF3B-F59F-45C2-827C-821C669E38A3}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">WT3000 Lib.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
